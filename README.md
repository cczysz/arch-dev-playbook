# Ansible Project

This repository contains an Ansible project for managing infrastructure.

## Getting Started

To get started with this project, follow these steps:

1. Clone the repository: `git clone <repository-url>`
2. Install Ansible: `pip install ansible`
3. Configure your inventory: Edit the `inventory.ini` file to specify your target hosts.
4. Run the playbook: `ansible-playbook playbook.yml`

## Project Structure

The project structure is as follows:

